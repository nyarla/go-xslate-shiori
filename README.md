shiori
======

[![Build Status](https://travis-ci.org/nyarla/go-xslate-shiori.svg?branch=master)](https://travis-ci.org/nyarla/go-xslate-shiori) [![GoDoc](http://godoc.org/github.com/nyarla/go-xslate-shiori?status.svg)](http://godoc.org/github.com/nyarla/go-xslate-shiori)


A template fetcher for [go-xslate](https://github.com/lestrrat/go-xslate) with [go-bindata](https://github.com/jteeuwen/go-bindata)

Usage
-----

### compile to go code from template source.

```bash
# for example
$ go-bindata -o templates.go -pkg "myapp" -ignore=\\.DS_Store tpls/...
```

### using

```
package myapp

import (
    "github.com/lestrrat/go-xslate"
    "github.com/nyarla/go-xslate-shiori"
)

func main() {
    tt, err := xslate.New(xslate.Args{
		"ConfigureLoader": ShioriConfigureLoader,
		"Parser": xslate.Args{
			"Syntax": "TTerse",
		},
		"Loader": xslate.Args{
			"Asset": testdata.Asset,
		},
	})

    if err != nil {
        panic(err)
    }

    // your code with `tt` is here.
}

```
 
Documentation
-------------

<http://godoc.org/github.com/nyarla/go-xslate-shiori>

Author
------

Naoki OKAMURA a.k.a nyarla <nyarla@thotep.net>
 
License
-------

[MIT](http://nyarla.mit-license.org/2014)

