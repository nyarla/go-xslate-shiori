package shiori

import (
	"github.com/lestrrat/go-xslate"
	"github.com/nyarla/go-xslate-shiori/testdata"
	"testing"
)

func TestShiori(t *testing.T) {
	tt, err := xslate.New(xslate.Args{
		"ConfigureLoader": ShioriConfigureLoader,
		"Parser": xslate.Args{
			"Syntax": "TTerse",
		},
		"Loader": xslate.Args{
			"Asset": testdata.Asset,
		},
	})

	if err != nil {
		t.Fatal(err)
	}

	ret, err := tt.Render(`template.tt`, xslate.Vars{"name": "shiori"})
	if err != nil {
		t.Fatal(err)
	}

	if ret != "hello shiori\n" {
		t.Errorf(`Result of Render template does not match: "%s" != "hello shiori\\n"`, ret)
	}
}
