package shiori

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"time"

	"github.com/lestrrat/go-xslate"
	"github.com/lestrrat/go-xslate/loader"
)

var compiledTime = time.Now()

var (
	ErrAssetIsNotFound       = errors.New(`shiori: Asset func is not found.`)
	ErrInvalidAssetInterface = errors.New(`shiori: 'Asset' value does not match of shiori.Asset or shiori.AssetFunc.`)
	ErrInvalidCacheArgs      = errors.New(`shiori: 'Cache' value does not match of string, fmt.Stringer or xslate's loader.Cache`)
	ErrInvalidCacheLevel     = errors.New(`shiori: 'CacheLevel' value is invalid.`)
)

type AssetFunc func(name string) ([]byte, error)

func (a AssetFunc) Asset(name string) ([]byte, error) {
	return a(name)
}

type Asset interface {
	Asset(name string) ([]byte, error)
}

type fetcher struct {
	loader Asset
}

func (f *fetcher) FetchTemplate(name string) (loader.TemplateSource, error) {
	if val, err := f.loader.Asset(name); err != nil {
		return nil, loader.ErrTemplateNotFound
	} else {
		return &source{data: val}, nil
	}
}

type source struct {
	data []byte
}

func (s *source) LastModified() (time.Time, error) {
	return compiledTime, nil
}

func (s *source) Bytes() ([]byte, error) {
	return s.data, nil
}

func (s *source) Reader() (io.Reader, error) {
	return bytes.NewReader(s.data), nil
}

func ShioriConfigureLoader(tx *xslate.Xslate, args xslate.Args) error {
	var asset Asset
	if val, ok := args.Get(`Asset`); !ok {
		return ErrAssetIsNotFound
	} else {
		switch val.(type) {
		case func(string) ([]byte, error):
			asset = AssetFunc(val.(func(string) ([]byte, error)))
		case AssetFunc:
			asset = val.(AssetFunc)
		case Asset:
			asset = val.(Asset)
		default:
			return ErrInvalidAssetInterface
		}
	}

	var cacheInstance loader.Cache
	if val, ok := args.Get(`Cache`); ok {
		switch val.(type) {
		case string:
			if cache, err := loader.NewFileCache(val.(string)); err != nil {
				return err
			} else {
				cacheInstance = cache
			}
		case fmt.Stringer:
			if cache, err := loader.NewFileCache(val.(fmt.Stringer).String()); err != nil {
				return err
			} else {
				cacheInstance = cache
			}
		case loader.Cache:
			cacheInstance = val.(loader.Cache)
		default:
			return ErrInvalidCacheArgs
		}
	} else {
		cacheInstance = make(loader.MemoryCache)
	}

	var cacheLevel loader.CacheStrategy
	if val, ok := args.Get(`CacheLevel`); !ok {
		cacheLevel = 1
	} else {
		if data, ok := val.(loader.CacheStrategy); !ok {
			return ErrInvalidCacheLevel
		} else {
			cacheLevel = data
		}
	}

	tx.Loader = loader.NewCachedByteCodeLoader(cacheInstance, cacheLevel, &fetcher{asset}, tx.Parser, tx.Compiler)
	return nil
}
